#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive

sudo wget https://dev.mysql.com/get/mysql-apt-config_0.6.0-1_all.deb

echo mysql-apt-config mysql-apt-config/repo-distro select ubuntu | debconf-set-selections
echo mysql-apt-config mysql-apt-config/repo-codename select trusty | debconf-set-selections
echo mysql-apt-config mysql-apt-config/select-server select mysql-5.7 | debconf-set-selections
echo mysql-community-server mysql-community-server/root-pass  | debconf-set-selections
echo mysql-community-server mysql-community-server/re-root-pass  | debconf-set-selections

sudo dpkg -i mysql-apt-config_0.6.0-1_all.deb

sudo apt-get update
sudo apt-get install -y mysql-server
