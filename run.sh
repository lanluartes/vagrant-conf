#!/bin/bash

$DIRECTORY = "/home/vagrant/doggl"

if [ ! -d $DIRECTORY ]; then
  echo "cloning project..."
  git clone https://lanluartes:86Jx1fFzyojrZDgK-C9n@gitlab.com/lanluartes/doggl.git
fi

# installing dependencies
cd doggl && sudo npm install && sudo npm install -g concurrently

# running the app
echo "You can now access the app at localhost:8080"

npm run dev &
