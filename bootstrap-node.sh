#!/bin/bash

# i need to add the auto run
#  [sadly i still can't find a solution] - spin up new vm with needed pre-requisites
#  [done] - run app automatically
#  [i still cant find a work around] - must have a database

# $DIRECTORY = "/home/vagrant/doggl"
$DATABASE = "/var/lib/mysql/todo"

#getting updated lts version of npm and nodejs
echo "GETTING UPDATED VERSION OF NODEJS..."
sudo apt-get install curl python- software-properties -yq &> /dev/null
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -

# simple update and upgrade
echo "updating and upgrading..."
sudo apt-get update -yq &>/dev/null
sudo apt-get upgrade -yq &>/dev/null

# installing mysql
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password s3cr3t'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password s3cr3t'
sudo apt-get -y install mysql-server

#creating database
mysql -u root -ps3cr3t -e "create database todo"

mysql -u root -ps3cr3t todo < '/vagrant/todo.sql'
# ^ for importing database

# sudo apt-get install mysql-server -y

if hash git 2>/dev/null ; then
  echo "git is alreay installed..."
else
  echo "installing git..."
  sudo apt-get install git -yq
fi

# installing pre-reqs
if hash nodejs 2> /dev/null ; then
  echo "nodejs and npm is already installed..."
else
  echo "installing nodejs and npm..."

  sudo apt-get install nodejs -yq
fi

# git clone if project is not existing
# if [ ! -d "$DIRECTORY" ]; then
#   echo "cloning project..."
#   git clone https://lanluartes:86Jx1fFzyojrZDgK-C9n@gitlab.com/lanluartes/doggl.git
# fi
#
# # installing dependencies
# cd doggl && sudo npm install && sudo npm install -g concurrently
#
# # running the app
# echo "You can now access the app at localhost:8080"
#
# npm run dev &


# sudo apt-get install mysql-server -yq
